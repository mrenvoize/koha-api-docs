# koha-api-docs

This project takes care of building the REST API docs for official publishing.

It includes the following releases
- 19.11
- 20.05
- 20.11
- 21.05
