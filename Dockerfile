# Build the HTML files
FROM node:12-alpine as build

LABEL org.opencontainers.image.authors="Tomás Cohen Arazi <tomascohen@theke.io>"
LABEL description="Docker image for hosting the Koha REST API documentation"

# Add git and redoc-cli
RUN apk --no-cache add git
# Latest is 0.11.4, but breaks with our spec
RUN npm install -g redoc-cli@0.10.4

RUN mkdir /build
RUN mkdir /workdir
WORKDIR /workdir

## 19.11.x
# Fetch the latest tag
RUN export KOHA_TAG=$(git ls-remote --refs --tags \
                        --sort=version:refname https://git.koha-community.org/Koha-community/Koha.git \
                       | cut -f2 | grep v19.11 | tail -n 1 | cut -d'/' -f 3) ; \
    git clone --depth 1 https://git.koha-community.org/Koha-community/Koha.git --branch ${KOHA_TAG} --single-branch 19.11
# Generate the API docs
RUN redoc-cli bundle \
      --cdn \
      --output /build/19.11.html \
      19.11/api/v1/swagger/swagger.json

## 20.05.x
# Fetch the latest tag
RUN export KOHA_TAG=$(git ls-remote --refs --tags \
                        --sort=version:refname https://git.koha-community.org/Koha-community/Koha.git \
                       | cut -f2 | grep v20.05 | tail -n 1 | cut -d'/' -f 3) ; \
    git clone --depth 1 https://git.koha-community.org/Koha-community/Koha.git --branch ${KOHA_TAG} --single-branch 20.05
# Generate the API docs
RUN redoc-cli bundle \
      --cdn \
      --output /build/20.05.html \
      20.05/api/v1/swagger/swagger.json

## 20.11.x
# Fetch the latest tag on the 20.11.x branch
RUN export KOHA_TAG=$(git ls-remote --refs --tags \
                        --sort=version:refname https://git.koha-community.org/Koha-community/Koha.git \
                       | cut -f2 | grep v20.11 | tail -n 1 | cut -d'/' -f 3) ; \
    git clone --depth 1 https://git.koha-community.org/Koha-community/Koha.git --branch ${KOHA_TAG} --single-branch 20.11
# Generate the API docs
RUN redoc-cli bundle \
      --cdn \
      --output /build/20.11.html \
      20.11/api/v1/swagger/swagger.json

## 21.05.x
# Fetch the latest tag on the 20.11.x branch
# FIXME: Do the same as with other branches once the RM tags the release (even RC would work)
RUN git clone --depth 1 https://git.koha-community.org/Koha-community/Koha.git --branch master --single-branch 21.05
# Generate the API docs
RUN redoc-cli bundle \
      --cdn \
      --output /build/21.05.html \
      21.05/api/v1/swagger/swagger.yaml

# Get rid of the previous layers, just keep the /build volume
FROM nginx

COPY --from=build /build /usr/share/nginx/html
